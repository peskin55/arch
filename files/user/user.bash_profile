alias 'ls=ls --color'
alias 'la=ls -a'
alias 'll=la -lh'
alias 'du=du -h'
alias 'rm=rm -v'
alias 'mkdir=mkdir -pv'
alias 'rmdir=rmdir -v'
alias 'cp=cp -Rnv'
alias 'rsync=rsync -Pavr'
alias 'mv=mv -v'
alias 'mmv=mmv -v'
alias 'xclip=xclip -sel clip'

mkcd() {
  /bin/mkdir -pv -- "$1" &&
  cd -P -- "$1"
}
gitcd() {
  if test "${#2}" != 0; then
    git clone -- "$1" "$2" &&
    cd -P -- "$2"
  else
    dir="${1##*/}"
    git clone -- "$1" &&
    cd -P -- "${dir%.*}"
  fi
}
gitpush() {
  git commit -m "commit" &&
  git push
}
ffcrf() {
  ffmpeg -i "$1" -c:v libx264 -c:a aac -crf 23 "$2"/"${1%.*}".mp4
}
gitaur() {
  if test "${#2}" != 0; then
    git clone -- https://aur.archlinux.org/"$1".git "$2" &&
    cd -P -- "$2"
  else
    git clone -- https://aur.archlinux.org/"$1".git &&
    cd -P -- "$1"
  fi
}
vpnc() {
  if test "${#1}" != 0; then
    expressvpn connect "$1"
  else
    expressvpn connect
  fi
}
vpnd() {
  expressvpn disconnect &&
  sudo systemctl restart NetworkManager.service
}
check-makefile() {
	make -qp | awk -F':' '/^[a-zA-Z0-9][^$#\/\t=]*:([^=]|$)/ {split($1,A,/ /);for(i in A)print A[i]}' | sort -u
}
restart-anydesk() {
  sudo killall -9 anydesk &&
  sudo systemctl restart anydesk.service
}
chemby() {
  sudo chown -Rc zach:emby /Videos
}
export -f mkcd gitcd gitpush ffcrf gitaur vpnc vpnd check-makefile restart-anydesk chemby
source "$HOME"/.profile
