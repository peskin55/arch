alias 'ls=ls --color'
alias 'la=ls -a'
alias 'll=la -lh'
alias 'du=du -h'
alias 'rm=rm -v'
alias 'rmdir=rmdir -v'
alias 'cp=cp -Rnv'
alias 'mv=mv -v'
alias 'mmv=mmv -v'
function mkcd() {
  mkdir -p -- "$1" &&
  cd -P -- "$1"
}
function gitcd() {
  if test "${#2}" != 0; then
    git clone -- "$1" "$2" &&
    cd -P -- "$2"
  else
    dir="${1##*/}"
    git clone -- "$1" &&
    cd -P -- "${dir%.*}"
  fi
}
function gitpush() {
  git commit -m "commit" &&
  git push
}
function ffcrf() {
  ffmpeg -i "$1" -c:v libx264 -c:a aac -crf 23 "$2"/"${1%.*}".mp4
}
function gitaur() {
  if test "${#2}" != 0; then
    git clone -- https://aur.archlinux.org/"$1".git "$2" &&
    cd -P -- "$2"
  else
    git clone -- https://aur.archlinux.org/"$1".git &&
    cd -P -- "$1"
  fi
}
function vpnc() {
  if test "${#1}" != 0; then
    expressvpn connect "$1"
  else
    expressvpn connect
  fi
}
function vpnd() {
  expressvpn disconnect &&
  sudo systemctl restart NetworkManager.service
}
export -f mkcd gitcd gitpush ffcrf gitaur vpnc vpnd
source "$HOME"/.profile
