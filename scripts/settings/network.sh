sudo iptables -t nat -A POSTROUTING -s 10.8.0.0/24 -o wlp2s0 -j MASQUERADE && \
sudo dhcpcd && \
echo 'tun0' | sudo tee /etc/modules-load.d/tun0.conf && \
sudo systemctl enable dhcpcd && \
sudo systemctl enable NetworkManager && \
sudo systemctl enable expressvpn.service && \
sudo systemctl enable bluetooth && \
sudo systemctl enable teamviewerd.service && \
sudo systemctl enable anydesk.service
