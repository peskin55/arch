#!/bin/sh
set -x

"$(which pacman-key)" -r \
	8A8F901A \
&& \
"$(which pacman-key)" --lsign-key \
	8A8F901A \
&& \
"$(which pacman)" -Syy
