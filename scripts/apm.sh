#!/bin/sh
set -x

"$(which apm)" \
	i \
		atom-beautify \
		atom-material-syntax \
		atom-material-syntax-dark \
		atom-material-ui \
		autocomplete-latex \
		autocomplete-paths \
		busy-signal \
		dbg \
		dbg-gdb \
		file-icons \
		fortran-coverpage-syntax \
		hypest-dark \
		language-diff \
		language-fish-shell \
		language-generic-config \
		language-gentoo \
		language-ini \
		language-latex \
		language-meson \
		latex \
		monokai \
		one-dark-regex-syntax \
		output-panel \
		seti-syntax \
		seti-ui \
		terminal-tab \
		terminus \
		the-construct-syntax
