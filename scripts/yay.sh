yay -Syu --needed \
aur/dropbox \
aur/drive-bin \
aur/anydesk \
aur/gnome-terminal-fedora \
aur/authenticator \
aur/bitwarden \
aur/expressvpn \
aur/debtap \
aur/nomachine \
aur/nautilus-admin \
aur/nautilus-bluetooth \
aur/thunar-shares-plugin \
aur/nemo-compare \
aur/cliqz-bin \
aur/vivaldi \
aur/vivaldi-ffmpeg-codecs \
aur/mmv \
aur/plex-media-server
