#!/bin/bash
set -x

"$(which yay)" -Syyu --needed \
	dropbox \
	anydesk \
	wps-office \
	downgrade \
	rar \
	spotify \
	megasync \
	aria2-fast \
	pamac-aur-git \
	authenticator \
	ttf-wps-fonts \
	mailspring-libre \
	bitwarden \
	expressvpn \
	checkupdates+aur \
	nano-syntax-highlighting-git \
	nautilus-admin \
	zoom \
	virtualbox-ext-oracle \
	nautilus-bluetooth \
	thunar-shares-plugin \
	nemo-compare \
	vivaldi \
	vivaldi-ffmpeg-codecs \
	mmv \
	plex-media-server
